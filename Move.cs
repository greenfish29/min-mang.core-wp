﻿namespace Min_Mang.Core.WP
{
    public class Move
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="from">Index of field, from which we move.</param>
        /// <param name="to">Index of field, where we move.</param>
        public Move(int from, int to)
        {
            this.From = from;
            this.To = to;
        }

        public int From { get; set; }
        public int To { get; set; }
        public int Valuation { get; set; }
        public int[] Captured { get; set; }

        public override string ToString()
        {
            string text = "";
            text += ToCoordinates(this.From);
            text += "  -->  ";
            text += ToCoordinates(this.To);
            return text;
        }

        private static string ToCoordinates(int index)
        {
            string text = "";

            switch (index % 9)
            {
                case 0:
                    text += "A";
                    break;
                case 1:
                    text += "B";
                    break;
                case 2:
                    text += "C";
                    break;
                case 3:
                    text += "D";
                    break;
                case 4:
                    text += "E";
                    break;
                case 5:
                    text += "F";
                    break;
                case 6:
                    text += "G";
                    break;
                case 7:
                    text += "H";
                    break;
                case 8:
                    text += "I";
                    break;
            }

            switch (index / 9)
            {
                case 0:
                    text += "9";
                    break;
                case 1:
                    text += "8";
                    break;
                case 2:
                    text += "7";
                    break;
                case 3:
                    text += "6";
                    break;
                case 4:
                    text += "5";
                    break;
                case 5:
                    text += "4";
                    break;
                case 6:
                    text += "3";
                    break;
                case 7:
                    text += "2";
                    break;
                case 8:
                    text += "1";
                    break;
            }

            return text;
        }
    }
}
