﻿using System;
using System.Collections.Generic;

namespace Min_Mang.Core.WP
{
    public static class Referee
    {
        /// <summary>
        /// Check, if the move is in game desk indexes range.
        /// </summary>
        /// <param name="index">Field index.</param>
        private static bool ItsMoveOnDesk(GameDesk desk, int index)
        {
            return (index < desk.Size && index >= 0);
        }

        /// <summary>
        /// Check, if the move is between neighboring fields.
        /// </summary>
        /// <param name="from">Field index, from which we move.</param>
        /// <param name="where">Field index, where we move.</param>
        private static bool ItsMoveNeighboring(GameDesk desk, int from, int where)
        {
            return ((from % desk.Width == where % desk.Width || from / desk.Width == where / desk.Width) && (Math.Abs(from - where) == 1 || Math.Abs(from - where) == desk.Width));
        }

        /// <summary>
        /// Check, if the field, where we try move, is empty.
        /// </summary>
        /// <param name="desk">Game desk.</param>
        /// <param name="where">Field index, where we try move.</param>
        private static bool IsFieldEmpty(GameDesk desk, int where)
        {
            if (ItsMoveOnDesk(desk, where))
                return (desk.GetFieldValue(where) == FieldType.EMPTY);
            else
                return false;
        }

        public static bool IsSkip(GameDesk desk, Move t)
        {
            if ((t.From % desk.Width == t.To % desk.Width || t.From / desk.Width == t.To / desk.Width) &&
                (Math.Abs(t.From - t.To) == 2 || Math.Abs(t.From - t.To) == (desk.Width*2)) &&
                (!IsFieldEmpty(desk, (t.From + t.To) / 2)))
                return true;
            else
                return false;
        }

        /// <summary>
        /// Get possible moves from specific possition (4 direction check).
        /// </summary>
        /// <param name="desk">Game desk.</param>
        /// <param name="from">Field, from which we will move.</param>
        /// <returns>Possible moves.</returns>
        public static Move[] GetPossibleMoves(GameDesk desk, int from)
        {
            List<Move> temp = new List<Move>();

            Move newMove = new Move(from, from + desk.Width);
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);

            newMove = new Move(from, from - desk.Width);
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);

            newMove = new Move(from, from - 1);
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);

            newMove = new Move(from, from + 1);
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);

            return temp.ToArray();
        }

        public static Move[] PossibleSkipMoves(GameDesk desk, int from)
        {
            List<Move> temp = new List<Move>();

            Move newMove = new Move(from, from + desk.Width);
            Move t = new Move(from, from + (desk.Width*2));
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);
            else if (ValidateSkip(desk, t) && !IsFieldEmpty(desk, from + desk.Width) && desk.GetFieldValue(from) != desk.GetFieldValue(newMove.To))
            {
                if (IsSkip(desk, t))
                    temp.Add(t);
            }

            newMove = new Move(from, from - desk.Width);
            t = new Move(from, from - (desk.Width*2));
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);
            else if (ValidateSkip(desk, t) && !IsFieldEmpty(desk, from - desk.Width) && desk.GetFieldValue(from) != desk.GetFieldValue(newMove.To))
            {
                if (IsSkip(desk, t))
                    temp.Add(t);
            }

            newMove = new Move(from, from - 1);
            t = new Move(from, from - 2);
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);
            else if (ValidateSkip(desk, t) && !IsFieldEmpty(desk, from - 1) && desk.GetFieldValue(from) != desk.GetFieldValue(newMove.To))
            {
                if (IsSkip(desk, t))
                    temp.Add(t);
            }

            newMove = new Move(from, from + 1);
            t = new Move(from, from + 2);
            if (ValidateMove(desk, newMove))
                temp.Add(newMove);
            else if (ValidateSkip(desk, t) && !IsFieldEmpty(desk, from + 1) && desk.GetFieldValue(from) != desk.GetFieldValue(newMove.To))
            {
                if (IsSkip(desk, t))
                    temp.Add(t);
            }

            return temp.ToArray();
        }

        /// <summary>
        /// Get all possible moves from specified player.
        /// </summary>
        /// <param name="desk">Game desk.</param>
        /// <param name="fieldType">Player type.</param>
        /// <returns>All player possible moves.</returns>
        public static Move[] GetAllPossibleMoves(GameDesk desk, FieldType fieldType)
        {
            Move[] temp;
            List<Move> result = new List<Move>();

            for (int i = 0; i < desk.Size; i++)
            {
                if (desk.GetFieldValue(i) == fieldType)
                {
                    temp = GetPossibleMoves(desk, i);
                    foreach (Move t in temp)
                    {
                        result.Add(t);
                    }
                }
            }

            return result.ToArray();
        }

        public static Move[] GetAllPossibleSkipMoves(GameDesk desk, FieldType fieldType)
        {
            Move[] temp;
            List<Move> result = new List<Move>();

            for (int i = 0; i < desk.Size; i++)
            {
                if (desk.GetFieldValue(i) == fieldType)
                {
                    temp = PossibleSkipMoves(desk, i);
                    foreach (Move t in temp)
                    {
                        result.Add(t);
                    }
                }
            }

            return result.ToArray();
        }

        /// <summary>
        /// Check, if the move is valid.
        /// </summary>
        /// <param name="desk">Game desk.</param>
        /// <param name="move">Move.</param>
        public static bool ValidateMove(GameDesk desk, Move move)
        {
            int from = move.From;
            int where = move.To;
            return ((from != where) && ItsMoveOnDesk(desk,where) && ItsMoveOnDesk(desk,from) && ItsMoveNeighboring(desk,from, where)
                     && IsFieldEmpty(desk, where));
        }

        public static bool ValidateSkip(GameDesk desk, Move move)
        {
            int from = move.From;
            int where = move.To;
            return ((from != where) && ItsMoveOnDesk(desk,where) && ItsMoveOnDesk(desk,from) && IsFieldEmpty(desk, where));
        }

        /// <summary>
        /// Find all pieces, which one must be captured.
        /// </summary>
        /// <param name="desk">Game desk.</param>
        /// <param name="t">Move</param>
        /// <returns>All captured field indexes.</returns>
        public static int[] GetCapturedIndexes(GameDesk desk, Move t)
        {
            List<int> indexes = new List<int>();
            Move[] fields;

            int index = t.To;

            // If neighboring piece has no neighbors, it's candidate to capture.
            if (ItsMoveNeighboring(desk,index, index + 1) && ItsMoveOnDesk(desk,index + 1) && desk.GetFieldValue(index) != desk.GetFieldValue(index + 1))
            {
                fields = GetPossibleMoves(desk, index + 1);
                if (fields.Length == 0)
                    indexes.Add(index + 1);
            }

            if (ItsMoveNeighboring(desk,index, index - 1) && ItsMoveOnDesk(desk,index - 1) && desk.GetFieldValue(index) != desk.GetFieldValue(index - 1))
            {
                fields = GetPossibleMoves(desk, index - 1);
                if (fields.Length == 0)
                    indexes.Add(index - 1);
            }

            if (ItsMoveOnDesk(desk, index + desk.Width) && desk.GetFieldValue(index) != desk.GetFieldValue(index + desk.Width))
            {
                fields = GetPossibleMoves(desk, index + desk.Width);
                if (fields.Length == 0)
                    indexes.Add(index + desk.Width);
            }

            if (ItsMoveOnDesk(desk, index - desk.Width) && desk.GetFieldValue(index) != desk.GetFieldValue(index - desk.Width))
            {
                fields = GetPossibleMoves(desk, index - desk.Width);
                if (fields.Length == 0)
                    indexes.Add(index - desk.Width);
            }

            // Ignore not valid indexes from topmost and undermost line.
            for (int i = 0; i < indexes.Count; i++)
            {
                if ((indexes[i] < 0) || (indexes[i] > desk.Size-1))
                {
                    indexes.Remove(indexes[i]);
                    i--;
                }
            }

            // Ignore new empty indexes.
            for (int i = 0; i < indexes.Count; i++)
            {
                if (desk.GetFieldValue(indexes[i]) == FieldType.EMPTY)
                {
                    indexes.Remove(indexes[i]);
                    i--;
                }
            }

            if (indexes.Count == 0) return null;

            return indexes.ToArray();
        }

        public static int[] GetSkipCapturedIndexes(GameDesk desk, Move t)
        {
            List<int> indexes = new List<int>();

            if (!ItsMoveNeighboring(desk, t.From, t.To))
            {
                int index = (t.From + t.To) / 2;
                indexes.Add(index);
            }

            return indexes.ToArray();
        }

        /// <summary>
        /// Check if is game over.
        /// </summary>
        /// <param name="desk">Game desk.</param>
        /// <returns>True, if game over, else false.</returns>
        public static bool GameOver(GameDesk desk)
        {
            int p1count=desk.CountPieces(FieldType.P_ONE);
            int p2count=desk.CountPieces(FieldType.P_TWO);

            if ((desk.CountPieces(FieldType.P_ONE) == 0 || desk.CountPieces(FieldType.P_TWO) == 0) ||
                (p1count<4 && p2count<4))
                return true;
            else
                return false;
        }

    }
}
