﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Min_Mang.Core.WP
{
    public static class Minimax
    {
        /// <summary>
        /// Get random move.
        /// </summary>
        /// <param name="moves">Moves array.</param>
        /// <returns>Random move.</returns>
        public static Move Random(List<Move> moves)
        {
            Random rand = new Random();
            int randValue = rand.Next(0, moves.Count);
            return moves[randValue];
        }

        public static Move WorkMinimax(GameDesk desk, FieldType player, int depth, bool minOrMax)
        {
            bool alone = desk.CountPieces(player) == 1;
            Move[] moves;

            if (alone)
            {
                moves = Referee.GetAllPossibleSkipMoves(desk, player);
            }
            else
            {
                moves = Referee.GetAllPossibleMoves(desk, player);
            }

            foreach (Move t in moves)
            {
                GameDesk actualDesk = desk.DeepCopy();
                actualDesk.Move(t);

                int[] capturedIndexes;
                if (alone)
                {
                    capturedIndexes = Referee.GetCapturedIndexes(actualDesk, t);
                    if (capturedIndexes != null)
                    {
                        List<int> tempCapt = capturedIndexes.ToList();
                        foreach (int i in Referee.GetSkipCapturedIndexes(actualDesk, t))
                        {
                            tempCapt.Add(i);
                        }
                        capturedIndexes = tempCapt.ToArray();
                    }
                }
                else
                {
                    capturedIndexes = Referee.GetCapturedIndexes(actualDesk, t);
                }

                actualDesk.Capture(capturedIndexes);
                if (depth > 1)
                    t.Valuation = WorkMinimax(actualDesk,
                      (player == FieldType.P_ONE ? FieldType.P_TWO : FieldType.P_ONE),
                      depth - 1, (minOrMax ? false : true)).Valuation;
                else
                    Evaluate(actualDesk, t, player);
            }
            int value;
            if (minOrMax)
            {
                if (moves.Length == 0)
                {
                    Move t = new Move(0, 0);
                    t.Valuation = -1;
                    return t;
                }
                else
                    value = moves.Max<Move>(i => i.Valuation);
            }
            else
            {
                if (moves.Length == 0)
                {
                    Move t = new Move(0, 0);
                    t.Valuation = 100;
                    return t;
                }
                else
                    value = moves.Min<Move>(i => i.Valuation);
            }
            List<Move> temp = new List<Move>();
            foreach (Move t in moves)
            {
                if (t.Valuation == value)
                    temp.Add(t);
            }
            return Random(temp);
        }

        private static void Evaluate(GameDesk desk, Move move, FieldType player)
        {
            int poneCount = desk.CountPieces(FieldType.P_ONE);
            int ptwoCount = desk.CountPieces(FieldType.P_TWO);

            if (player == FieldType.P_ONE)
                move.Valuation = 15 + poneCount - ptwoCount;
            else
                move.Valuation = 15 + ptwoCount - poneCount;
        }

    }
}
