﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Min_Mang.Core.WP
{
    [DataContract]
    public class GameDesk
    {
        /// <summary>
        /// Game fields array.
        /// </summary>
        [DataMember]
        public FieldType[] _state;
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        public int Size { get { return Width * Height; } }

        /// <summary>
        /// Moves history.
        /// </summary>
        private List<Move> _movesHistory = new List<Move>();
        public List<Move> MovesHistory
        {
            get { return _movesHistory; }
            set { _movesHistory = value; }
        }

        public GameDesk() { }

        /// <summary>
        /// Constructor, initialize game desk.
        /// </summary>
        /// <param name="width">Game desk width.</param>
        /// <param name="height">Game desk height.</param>
        public GameDesk(int width, int height)
        {
            Width = width;
            Height = height;
            _state = new FieldType[Size];
            for (int i = 0; i < Size; i++)
            {
                if (i % width == 0)
                    _state[i] = FieldType.P_ONE;
                else if (i % width == width - 1)
                    _state[i] = FieldType.P_TWO;
                else if (i / width == width - 1)
                    _state[i] = FieldType.P_TWO;
                else if (i / width == 0)
                    _state[i] = FieldType.P_ONE;
                else
                    _state[i] = FieldType.EMPTY;
            }
        }

        /// <summary>
        /// For testing only.
        /// </summary>
        /// <param name="s"></param>
        public GameDesk(string s, int width, int height)
        {
            Width = width;
            Height = height;
            _state = new FieldType[49];

            for (int i = 0; i < Size; i++)
            {
                _state[i] = FieldType.EMPTY;
            }
            //_state[3] = FieldType.P_ONE;
            //_state[12] = FieldType.P_ONE;
            //_state[11] = FieldType.P_ONE;
            //_state[10] = FieldType.P_ONE;
            _state[1] = FieldType.P_TWO;
            _state[0] = FieldType.P_ONE;
            _state[2] = FieldType.P_ONE;
            _state[3] = FieldType.P_ONE;
            _state[7] = FieldType.P_ONE;
            _state[9] = FieldType.P_ONE;
            _state[43] = FieldType.P_ONE;
            _state[8] = FieldType.P_TWO;
            _state[42] = FieldType.P_TWO;
        }

        public GameDesk DeepCopy()
        {
            GameDesk desk = new GameDesk(this.Width, this.Height);
            desk._state = new FieldType[Size];
            _state.CopyTo(desk._state, 0);
            return desk;
        }

        /// <summary>
        /// Return a state of the game field.
        /// </summary>
        /// <param name="index">Field index.</param>
        /// <returns>FieldType enum value.</returns>
        public FieldType GetFieldValue(int index)
        {
            return _state[index];
        }

        private void SetFieldValue(int index, FieldType fieldType)
        {
            _state[index] = fieldType;
        }

        public GameDesk Move(Move t)
        {
            this.SetFieldValue(t.To, this.GetFieldValue(t.From));
            this.SetFieldValue(t.From, FieldType.EMPTY);

            return this;
        }

        /// <summary>
        /// Return move back.
        /// </summary>
        /// <param name="t">Move.</param>
        /// <returns>Game desk.</returns>
        public GameDesk MoveBack(Move t)
        {
            this.SetFieldValue(t.From, this.GetFieldValue(t.To));
            this.SetFieldValue(t.To, FieldType.EMPTY);

            return this;
        }

        public void Capture(int[] fields)
        {
            if (fields != null)
            {
                foreach (int index in fields)
                {
                    this.SetFieldValue(index, FieldType.EMPTY);
                }
            }
        }

        public void CaptureBack(int[] field, FieldType fieldType)
        {
            fieldType = (fieldType == FieldType.P_ONE ? FieldType.P_TWO : FieldType.P_ONE);
            if (field != null)
            {
                foreach (int index in field)
                {
                    this.SetFieldValue(index, fieldType);
                }
            }
        }

        public void AddMoveToHistory(Move t)
        {
            MovesHistory.Add(t);
        }

        /// <summary>
        /// Count pieces of specific player.
        /// </summary>
        /// <param name="barva">Player, for which are pieces counted.</param>
        /// <returns>Number of player pieces.</returns>
        public int CountPieces(FieldType player)
        {
            return _state.Count(i => i == player);
        }
    }
}
