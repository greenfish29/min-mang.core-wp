﻿namespace Min_Mang.Core.WP
{
    public interface IDirector
    {
        #region Properties
        
        GameDesk GameDesk{get;}

        FieldType WhoMoves{get;}

        #endregion

        #region Methods

        void StartNewGame(GameDesk desk, PlayerType p1, PlayerType p2, Difficulty d1, Difficulty d2, GameState state, FieldType whoMoves);

        bool Move(Move m);

        #endregion
    }
}
