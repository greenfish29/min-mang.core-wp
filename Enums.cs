﻿namespace Min_Mang.Core.WP
{
    /// <summary>
    /// Field has always one of these states.
    /// </summary>
    public enum FieldType
    {
        P_ONE, P_TWO, EMPTY
    }

    /// <summary>
    /// Type of player.
    /// </summary>
    public enum PlayerType
    {
        HUMAN, COMPUTER
    }

    /// <summary>
    /// Computer player skill.
    /// </summary>
    public enum Difficulty
    {
        EASY = 1, NORMAL = 2, HARD = 3
    }

    /// <summary>
    /// State of game.
    /// </summary>
    public enum GameState
    {
        RUNNING, PAUSED, GAME_OVER
    }
}
