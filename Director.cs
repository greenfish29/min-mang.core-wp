﻿using System.Threading;
using System;
using System.Diagnostics;

namespace Min_Mang.Core.WP
{
    public sealed class Director : IDirector
    {
        #region Properties
        
        private static readonly Director _instance = new Director();
        private GameDesk _gameDesk;
        private AutoResetEvent _move = new AutoResetEvent(false);
        public delegate void PieceEventHandler(Move move, int[] capturedPieces);
        public event PieceEventHandler PieceMoved;
        public Move LastMove { get; private set; }
        private PlayerType _p1, _p2;
        private Difficulty _d1, _d2;
        private Thread _gameThread;
        private PlayerType _actualPlayer;

        public static Director Instance
        {
            get { return _instance; }
        }

        public GameDesk GameDesk 
        { 
            get { return _gameDesk; } 
        }

        public GameState GameState { get; set; }

        public FieldType WhoMoves { get; set; }

        #endregion

        #region Methods
        
        public void StartNewGame(GameDesk desk, PlayerType p1, PlayerType p2, Difficulty d1, Difficulty d2, GameState state, FieldType whoMoves)
        {
            _p1 = p1;
            _p2 = p2;
            _d1 = d1;
            _d2 = d2;

            _gameDesk = desk;
            GameState = state;
            WhoMoves = whoMoves;

            if (state == GameState.RUNNING)
            {
                _gameThread = new Thread(GameRunThread);
                _gameThread.Start();
            }
        }

        private void GameRunThread()
        {
            Debug.WriteLine("Thread started!");
            while (GameState != GameState.PAUSED)
            {
                if (Referee.GameOver(_gameDesk))
                {
                    GameState = GameState.GAME_OVER;
                    return;
                }

                _actualPlayer = WhoMoves == FieldType.P_ONE ? _p1 : _p2;

                if (_actualPlayer == PlayerType.COMPUTER)
                {
                    Thread.Sleep(1000);
                    var difficulty = WhoMoves == FieldType.P_ONE ? _d1 : _d2;
                    Move move = Minimax.WorkMinimax(_gameDesk.DeepCopy(), WhoMoves, (int)difficulty, true);
                    if (GameState == GameState.RUNNING)
                    {
                        Move(move);
                        _move.Reset();
                    }
                    else break;
                }
                else
                {
                    WaitHandle.WaitAny(new WaitHandle[] { _move });
                }

                WhoMoves = WhoMoves == FieldType.P_ONE ? FieldType.P_TWO : FieldType.P_ONE;
            }
        }

        public bool Move(Move m)
        {
            bool alone = _gameDesk.CountPieces(WhoMoves) == 1 ? true : false;
            int[] capturedPieces;

            if (!alone && Referee.ValidateMove(_gameDesk, m))
            {
                _gameDesk.Move(m);
                capturedPieces = Referee.GetCapturedIndexes(_gameDesk, m);
                _gameDesk.Capture(capturedPieces);
                if(_actualPlayer == PlayerType.HUMAN) _move.Set();
                PieceMoved(m, capturedPieces);
                return true;
            }
            else if (alone && Referee.ValidateSkip(_gameDesk, m))
            {
                _gameDesk.Move(m);
                capturedPieces = Referee.GetSkipCapturedIndexes(_gameDesk, m);
                _gameDesk.Capture(capturedPieces);
                if (_actualPlayer == PlayerType.HUMAN) _move.Set();
                PieceMoved(m, capturedPieces);
                return true;
            }

            return false;
        }

        public void Pause()
        {
            GameState = GameState.PAUSED;
        }

        public void Unpause()
        {
            GameState = GameState.RUNNING;
            _gameThread = new Thread(GameRunThread);
            _gameThread.Start();
        }

        #endregion
    }
}
